import { NavigationContainer } from '@react-navigation/native';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { StatusBar } from 'react-native';
import { darkGreen, primaryFontColor } from './src/configs/index';

//Pages
import PodCasts from './src/pages/PodCasts';
import Home from './src/pages/Home';

//PodCasts Pages
import PodCast01 from './src/pages/PodCast01';

// https://materialdesignicons.com

const App = () => {
  const Tab = createMaterialBottomTabNavigator();
  const Stack = createNativeStackNavigator();

  return (
    <NavigationContainer style={{ flex: 1 }}>
      <StatusBar hidden={true} />
      <Stack.Navigator
        hidden={true}
        screenOptions={{
          headerShown: false,
        }}
      >
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="PodCasts" component={PodCasts} />
        <Stack.Screen name="PodCast01" component={PodCast01} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
