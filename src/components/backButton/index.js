import { TouchableOpacity, View } from 'react-native';
import styles from './styles.js';
import Icon from 'react-native-vector-icons/Ionicons';

const BackButton = ({
  navigation,
  backShow,
  controlContent,
  setControlContent,
}) => {
  const controlChangeUp = () => {
    if (controlContent + 1 > 2) {
      setControlContent(2);
    } else {
      setControlContent(controlContent + 1);
    }
  };

  const controlChangeDown = () => {
    if (controlContent - 1 < 1) {
      setControlContent(1);
    } else {
      setControlContent(controlContent - 1);
    }
  };

  return (
    <View style={styles.container}>
      {backShow ? (
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={styles.buttonBack}
        >
          <Icon name="return-up-back" size={25} color={'#fff'} />
        </TouchableOpacity>
      ) : (
        <View />
      )}

      <View style={styles.slideContoller}>
        <TouchableOpacity
          onPress={() => controlChangeDown()}
          style={styles.buttonSlideLeft}
        >
          <Icon name="chevron-back-outline" size={22} color={'#fff'} />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => controlChangeUp()}
          style={styles.buttonSlideRight}
        >
          <Icon name="chevron-forward-outline" size={22} color={'#fff'} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default BackButton;
