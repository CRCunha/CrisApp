import { StyleSheet, Dimensions } from 'react-native';
import { green } from '../../configs/index';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default StyleSheet.create({
  container: {
    paddingLeft: 30,
    paddingRight: 25,
    width: width,
    marginTop: -27,

    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  buttonBack: {
    width: 58,
    height: 52,
    backgroundColor: `${green}`,
    borderRadius: 8,

    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',

    shadowColor: 'rgba(0,0,0,0.3)',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.43,
    shadowRadius: 9.51,
  },
  slideContoller: {
    marginLeft: 20,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end',

    shadowColor: 'rgba(0,0,0,0.3)',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.43,
    shadowRadius: 9.51,

    borderRadius: 8,
  },
  buttonSlideLeft: {
    width: 58,
    height: 52,
    backgroundColor: `${green}`,
    borderBottomLeftRadius: 8,
    borderTopLeftRadius: 8,

    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonSlideRight: {
    width: 58,
    height: 52,
    backgroundColor: `${green}`,
    borderBottomRightRadius: 8,
    borderTopRightRadius: 8,

    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
