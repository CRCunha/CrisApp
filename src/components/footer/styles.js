import { StyleSheet, Dimensions } from 'react-native';
import { gray } from '../../configs';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default StyleSheet.create({
  container: {
    width: width,
    display: 'flex',
    flex: 1,
    height: 70,
    backgroundColor: '#ebebeb',

    marginTop: 30,
  },
});
