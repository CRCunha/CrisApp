import { StyleSheet, Dimensions } from 'react-native';
import { green } from '../../configs/index';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default StyleSheet.create({
  container: {
    height: 400,
    display: 'flex',
    flex: 1,
  },
  imageBackGround: {
    height: 400,
    paddingLeft: 20,
    paddingRight: 20,

    display: 'flex',
    justifyContent: 'flex-end',
  },
  content: {
    display: 'flex',
    minHeight: 255,
  },
  dataContent: {
    color: '#fff',
    fontSize: 16,
    minHeight: 45,
  },
  dataTitle: {
    color: '#fff',
    fontSize: 37,
    minHeight: 75,
    fontWeight: 'bold',
  },
  dataSubTitle: {
    color: '#fff',
    width: width - 40,
    fontSize: 13,
    minHeight: 15,
    flexWrap: 'wrap',
  },
  button: {
    marginTop: 15,
    width: 110,
    height: 40,
    backgroundColor: `${green}`,
    borderRadius: 8,

    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  dataButton: {
    color: '#fff',
    fontSize: 16,
  },
});
