import React, { useState } from 'react';
import { View, ImageBackground, Text, TouchableOpacity } from 'react-native';
import styles from './styles.js';
import BackButton from '../backButton';

const Header = ({ navigation, backShow }) => {
  const [controlContent, setControlContent] = useState(1);
  const ImageBackgroundAsset = '../../../assets/background.jpg';

  const defaultContent = () => {
    return (
      <View style={styles.content}>
        <Text style={styles.dataContent}>31/03/2019</Text>
        <Text style={styles.dataTitle}>Download</Text>
        <Text style={styles.dataSubTitle}>
          Praesent euismod lacus nibh, quis pretium.
        </Text>
        <TouchableOpacity
          onPress={() => console.log('press')}
          style={styles.button}
        >
          <Text style={styles.dataButton}>Download</Text>
        </TouchableOpacity>
      </View>
    );
  };

  const defaultContent2 = () => {
    return (
      <View style={styles.content}>
        <Text style={styles.dataContent}>30/03/2019</Text>
        <Text style={styles.dataTitle}>New Title</Text>
        <Text style={styles.dataSubTitle}>
          Praesent euismod lacus nibh, quis pretium turpis finibus id.
        </Text>
      </View>
    );
  };

  const choiceContent = () => {
    if (controlContent == 1) {
      return defaultContent();
    }
    return defaultContent2();
  };

  return (
    <View style={styles.container}>
      <ImageBackground
        source={require(`${ImageBackgroundAsset}`)}
        resizeMode="cover"
        style={styles.imageBackGround}
      >
        {choiceContent()}
      </ImageBackground>
      <BackButton
        backShow={backShow}
        navigation={navigation}
        setControlContent={setControlContent}
        controlContent={controlContent}
      />
    </View>
  );
};

export default Header;
