import {
  StyleSheet,
  View,
  ImageBackground,
  Dimensions,
} from 'react-native';
import { Button } from 'react-native-paper';
import { blue } from '../../configs/index';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const Splash = ({ navigation }) => {
  const ImageBackgroundAsset = '../../../assets/startPage.png';
  return (
    <View style={styles.container}>
      <ImageBackground
        source={require(`${ImageBackgroundAsset}`)}
        resizeMode="cover"
        style={{ width: width }}
      >
        <View style={styles.content}>
          <View style={styles.buttonContainer}>
            <Button
              //icon="camera"
              style={styles.button}
              color={`${blue}`}
              onPress={() => navigation.navigate('Home')}
            >
              Entrar
            </Button>
          </View>
        </View>
      </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: width,
    height: height,
    display: 'flex',
    justifyContent: 'center',
  },
  buttonContainer: {
    width: width - 120,
    height: height - 100,

    display: 'flex',
    justifyContent: 'center',

    marginTop: 60,
  },
  button: {
    display: 'flex',
    justifyContent: 'center',
    height: 50,
    backgroundColor: '#fff',

    borderRadius: 10,
  },
  content: {
    width: width,
    height: height,

    display: 'flex',
    alignItems: 'center',
  },
});

export default Splash;
