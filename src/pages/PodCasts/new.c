#include <stdio.h>
#include <stdlib.h>

int main () 
{
	int valor = 0;
	int notas50 = 0;
	int notas20= 0;
	int notas10 = 0;
	int notas5 = 0;
	int notas2 = 0;
	int notas1 = 0;
	
	printf("Digite um valor inicial: \n");
	scanf("%i", &valor);
	
	if (valor >= 50) {
		notas50 = valor / 50;
		
		valor = valor - (50 * notas50);
	}
	if (valor >= 20) {
		notas20 = valor / 20;
		
		valor = valor - (20 * notas20);
	}
	if (valor >= 10) {
		notas10 = valor / 10;
		
		valor = valor - (10 * notas10);
	}
	if (valor >= 5) {
		notas5 = valor / 5;
		
		valor = valor - (5 * notas5);
	}
	if (valor >= 2) {
		notas5 = valor / 2;
		
		valor = valor - (2 * notas2);
	}
	if (valor >= 1) {
		notas5 = valor / 1;
		
		valor = valor - (1 * notas1);
	}
	printf("Notas de 50: %i \n Notas de 20: %i \n Notas de 10: %i \n Notas de 5: %i \n Notas de 2 %i \n Notas de 1 %i \n", &notas50, &notas20, &notas10, &notas5, &notas2, &notas1);
	
	return 0;
}