import {
  View,
  ScrollView,
  Text,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import styles from './styles.js';
import Header from '../../components/header';
import Footer from '../../components/footer';

// Images
const card1 = '../../../assets/cards/01.jpg';
const card2 = '../../../assets/cards/02.jpg';
const card3 = '../../../assets/cards/03.jpg';

const PodCasts = ({ navigation }) => {
  return (
    <ScrollView style={styles.container}>
      <Header navigation={navigation} backShow={true} />
      <View style={styles.content}>
        <Text style={styles.title}>PodCast's</Text>
        <Text style={styles.subTitle}>IFSul RPG</Text>
      </View>
      <View style={styles.buttonsContainer}>
        {/* Card 1 */}
        <TouchableOpacity
          onPress={() => navigation.navigate('PodCast01')}
          style={styles.button}
        >
          <ImageBackground
            source={require(`${card1}`)}
            resizeMode="cover"
            style={styles.imageBackGround}
            imageStyle={{ borderRadius: 8 }}
          >
            <Text style={styles.dataText}>20/09/2018</Text>
            <Text style={styles.dataTitle}>Cachorros do Demônio</Text>
          </ImageBackground>
        </TouchableOpacity>

        {/* Card 2 */}
        <TouchableOpacity style={styles.button}>
          <ImageBackground
            source={require(`${card2}`)}
            resizeMode="cover"
            style={styles.imageBackGround}
            imageStyle={{ borderRadius: 8 }}
          >
            <Text style={styles.dataText}>15/04/2017</Text>
            <Text style={styles.dataTitle}>A Escuridão da Alma</Text>
          </ImageBackground>
        </TouchableOpacity>

        {/* Card 3 */}
        <TouchableOpacity style={styles.button}>
          <ImageBackground
            source={require(`${card3}`)}
            resizeMode="cover"
            style={styles.imageBackGround}
            imageStyle={{ borderRadius: 8 }}
          >
            <Text style={styles.dataText}>30/06/2017</Text>
            <Text style={styles.dataTitle}>Cinza e Colorido</Text>
          </ImageBackground>
        </TouchableOpacity>
      </View>
      <Footer />
    </ScrollView>
  );
};

export default PodCasts;
