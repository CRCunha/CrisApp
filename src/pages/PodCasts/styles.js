import { StyleSheet, Dimensions } from 'react-native';
import { darkFont, green } from '../../configs';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default StyleSheet.create({
  container: {
    display: 'flex',
    flex: 1,
    height: '100%',
  },
  content: {
    paddingTop: 50,
    paddingLeft: 20,
    paddingRight: 20,
    flex: 1,

    marginBottom: 20,
  },
  title: {
    height: 40,
    fontSize: 24,
    color: `${darkFont}`,
  },
  subTitle: {
    fontSize: 12,
    color: `${darkFont}`,
  },
  buttonsContainer: {
    paddingLeft: 18,

    flex: 1,
  },
  button: {
    width: '100%',
    height: 200,
    borderRadius: 8,

    marginBottom: 40,
  },
  imageBackGround: {
    width: '100%',
    height: 200,
    paddingLeft: 25,
    paddingTop: 12,
    paddingBottom: 22,

    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  dataText: { color: '#fff', fontWeight: '600', fontSize: 16 },
  dataTitle: { color: '#fff', fontWeight: '600', fontSize: 22 },
});
