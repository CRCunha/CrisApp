import { StyleSheet, Dimensions } from 'react-native';
import { darkFont, green } from '../../configs';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default StyleSheet.create({
  container: {
    display: 'flex',
    flex: 1,
    height: '100%',
  },
  content: {
    paddingTop: 50,
    paddingLeft: 20,
    paddingRight: 20,
    flex: 1,
  },

  contentInfos: {
    paddingTop: 30,
    paddingLeft: 20,
    paddingRight: 20,
    flex: 1,

    marginBottom: 20,
  },
  title: {
    height: 40,
    fontSize: 24,
    color: `${darkFont}`,
  },
  subTitle: {
    fontSize: 12,
    color: `${darkFont}`,
  },
  PodCast01: {
    paddingLeft: 18,

    flex: 1,
  },
});
