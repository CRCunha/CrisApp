import React, { useEffect, useState } from 'react';
import { View, ScrollView, Text, Dimensions, Button } from 'react-native';
import styles from './styles.js';
import Header from '../../components/header';
import Footer from '../../components/footer';

import AudioSlider from '../../components/Player/AudioSlider';
import AudioFile from '../../../assets/audios/audio.mp3';

const width = Dimensions.get('window').width;

const PodCast01 = ({ navigation }) => {
  return (
    <ScrollView style={styles.container}>
      <Header navigation={navigation} backShow={true} />
      <View style={styles.content}>
        <Text style={styles.title}>Cachorros do Demônio</Text>
        <Text style={styles.subTitle}>RPG - 01</Text>
      </View>
      <View style={styles.contentInfos}>
        <AudioSlider audio={AudioFile} />
      </View>
      <Footer />
    </ScrollView>
  );
};

export default PodCast01;
