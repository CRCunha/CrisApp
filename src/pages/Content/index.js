import { View, Text, ImageBackground } from 'react-native';
import styles from './styles.js';
import BackButton from '../../components/backButton';

const Content = ({ navigation }) => {
  const ImageBackgroundAsset = '../../../assets/background.jpg';
  return (
    <View style={styles.container}>
      <ImageBackground
        source={require(`${ImageBackgroundAsset}`)}
        resizeMode="cover"
        style={styles.imageBackGround}
      >
        <BackButton navigation={navigation} />
      </ImageBackground>
      <View style={styles.content}>
        <View style={styles.topContent} />
        <Text>Content</Text>
      </View>
    </View>
  );
};

export default Content;
