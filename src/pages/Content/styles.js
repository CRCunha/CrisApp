import { StyleSheet, Dimensions } from 'react-native';
import { gray } from '../../configs/index';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default StyleSheet.create({
  container: {
    width: width,
    height: height,
    display: 'flex',
    justifyContent: 'flex-start',
  },
  content: {
    width: width,
    height: height,
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
    marginTop: -60,
    backgroundColor: '#fff',
    paddingTop: 50,
    paddingLeft: 30,
    paddingRight: 30,

    display: 'flex',
    alignItems: 'center',

    shadowColor: 'rgba(0,0,0,0.5)',
    shadowOffset: {
      width: 0,
      height: 7,
    },
    shadowOpacity: 0.43,
    shadowRadius: 9.51,

    elevation: 15,
  },
  topContent: {
    marginTop: -28,
    marginBottom: 20,
    width: 80,
    height: 5,
    borderRadius: 20,
    backgroundColor: `${gray}`,
  },
  imageBackGround: {
    paddingTop: 20,
    paddingLeft: 20,
    width: width,
    height: 260,
  },
});
