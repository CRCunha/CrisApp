import { StyleSheet, Dimensions } from 'react-native';
import { darkFont, green } from '../../configs';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default StyleSheet.create({
  container: {
    display: 'flex',
    flex: 1,
    height: '100%',
  },
  content: {
    paddingTop: 50,
    paddingLeft: 20,
    paddingRight: 20,
    flex: 1,

    marginBottom: 20,
  },
  title: {
    height: 40,
    fontSize: 24,
    color: `${darkFont}`,
  },
  subTitle: {
    fontSize: 12,
    color: `${darkFont}`,
  },
  text: {
    marginTop: 30,
    marginBottom: 40,
    fontSize: 14,
    textAlign: 'justify',
    color: `${darkFont}`,
  },
  buttonsContainer: {
    height: 150,
    padding: 15,

    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  button: {
    width: '46%',
    height: 130,

    backgroundColor: `${green}`,

    borderRadius: 8,

    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
