import { View, ScrollView, Text, TouchableOpacity } from 'react-native';
import styles from './styles.js';
import Header from '../../components/header';
import IconEntypo from 'react-native-vector-icons/Entypo';
import IconIonicons from 'react-native-vector-icons/Ionicons';
import IconFontisto from 'react-native-vector-icons/MaterialCommunityIcons';
import Footer from '../../components/footer';

const Home = ({ navigation }) => {
  return (
    <ScrollView style={styles.container}>
      <Header backShow={false} />
      <View style={styles.content}>
        <Text style={styles.title}>Bem Vindo Aventureiro</Text>
        <Text style={styles.subTitle}>IFSul RPG</Text>
        <Text style={styles.text}>
          Fundado por um grupo de amigos com uma paixão imensa por RPG de mesa,
          o grupo de RPG do IFSUL campus Peloats, se reúne no CTG todas as
          segundas de manhâ e de tarde para jogar as aventuras mais épicas e as
          historias mais emocionantes. Tendo como sistema primário D&D 5 edição,
          o grupo possui mestres com anos de experiência, fazendo fácil a
          entrada dos jogadores mais iniciantes, até os mais experiêntes.
          {'\n'}
          {'\n'}O que esta esperando? Pegue sua ficha e venha jogar conosco!
        </Text>
        <View style={styles.buttonsContainer}>
          <TouchableOpacity style={styles.button}>
            <IconEntypo name="book" size={50} color={'#fff'} />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => navigation.navigate('PodCasts')}
            style={styles.button}
          >
            <IconIonicons name="headset" size={50} color={'#fff'} />
          </TouchableOpacity>
        </View>
        <View style={styles.buttonsContainer}>
          <TouchableOpacity style={styles.button}>
            <IconIonicons name="game-controller" size={50} color={'#fff'} />
          </TouchableOpacity>
          <TouchableOpacity style={styles.button}>
            <IconFontisto name="discord" size={50} color={'#fff'} />
          </TouchableOpacity>
        </View>
      </View>
      <Footer />
    </ScrollView>
  );
};

export default Home;
