export const primaryFontColor = '#B2B5BA';
export const primaryFontColorStrong = '#939393';
export const darkFont = '#3f3f3f';

export const gray = '#F4F4F4';
export const blue = '#00C2CB';
export const green = '#6cda98';
